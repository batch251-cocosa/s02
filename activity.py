# [Activity]



year = input("Please input a year:\n")

if year.isdigit() is False:
	print("Please enter a valid input")
elif int(year) <= 0:
	print("Please enter a valid input")
else:
	leap_year = int(year) % 4

	if leap_year == 0:
		print(f"{year} is a leap year")
	else:
		print(f"{year} is not a leap year")


row = int(input("Enter number of rows\n"))
col = int(input("Enter number of columns\n"))

col2 = col

while row > 0:
	while col > 0:
		print("x", end=" ")
		col -= 1
	col = col2
	print("\n")
	row -= 1

